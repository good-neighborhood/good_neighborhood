# Benchmark des bases de données pour un site web PWA communautaire

Ce document présente un benchmark comparatif entre les bases de données PostgreSQL, Oracle, MySQL, SQL Server, et MongoDB.

## Critères de comparaison

- **Performance** : Temps de réponse des requêtes, capacité à gérer des charges lourdes.
- **Scalabilité** : Capacité de la base de données à se développer horizontalement ou verticalement.
- **Sécurité** : Fonctionnalités de gestion des permissions, chiffrement, etc.
- **Facilité d'intégration** : Compatibilité avec ExpressJS et ReactJS.
- **Coût** : Modèle de licence et coûts d'exploitation.
- **Communauté et support** : Nombre de ressources et qualité de la documentation.
- **Fonctionnalités spécifiques** : Gestion des relations complexes, types de données spécifiques, support des transactions, etc.


| Critères                    | PostGreSQL | MySQL | Oracle | SQL Server | MongoDB |
| --------------------------- | ---------- | ----- | ------ | ---------- | ------- |
| Performance                 | 9          | 8     | 7      | 7          | 8       |
| Scalabilité                 | 8          | 8     | 7      | 7          | 9       |
| Fonctionnalités spécifiques | 9          | 7     | 10     | 9          | 8       |
| Facilité d'intégration      | 8          | 9     | 6      | 7          | 9       |
| Communauté et Support       | 9          | 8     | 8      | 8          | 9       |
| Sécurité                    | 9          | 7     | 10     | 9          | 8       |
| Tarifs et license           | 10         | 9     | 3      | 6          | 9       |
| Total                       | 62         | 56    | 51     | 53         | 60      |



**Performance :**
- **PostgreSQL** est la meilleure en termes de gestion des requêtes complexes, idéal pour des applications nécessitant des jointures complexes et des transactions.
- **MongoDB** offre une bonne performance, particulièrement pour les applications avec des données moins structurées ou des charges variables.
- **MySQL** est rapide pour des requêtes simples et bien adapté pour des applications de taille modérée.
- **Oracle** et **SQL Server** sont performants dans des contextes d'entreprise, mais peuvent être moins adaptés pour des applications plus petites ou des charges légères.



**Scalabilité :**
- **MongoDB** est la meilleure en termes de scalabilité horizontale grâce au sharding, parfait pour les applications avec des données volumineuses ou des besoins de croissance rapide.
- **PostgreSQL** et **MySQL** offrent une bonne scalabilité verticale et, dans le cas de PostgreSQL, des extensions permettent d’améliorer la scalabilité horizontale.
- **Oracle** et **SQL Server** sont efficaces pour la scalabilité verticale, mais peuvent être plus complexes à mettre en œuvre pour une scalabilité horizontale.


**Sécurité :**
- **Oracle** est leader en termes de sécurité, avec des options de chiffrement avancées et un contrôle détaillé des accès.
- **SQL Server** et **PostgreSQL** offrent des fonctionnalités de sécurité robustes, y compris le chiffrement des données et des options de gestion des utilisateurs.
- **MongoDB** propose des options de sécurité solides, mais la gestion des transactions n'est pas aussi avancée.
- **MySQL** dispose d'options de sécurité de base, mais elles sont moins avancées par rapport aux autres options.



**Facilité d'intégration :**
- **MongoDB** et **MySQL** sont les plus faciles à intégrer avec **Node.js** et **ExpressJS**, grâce à leur large écosystème et modules comme **Mongoose** pour MongoDB et des ORM comme **Sequelize** pour MySQL.
- **PostgreSQL** est également bien supporté, mais sa configuration peut être légèrement plus complexe.
- **SQL Server** et **Oracle** sont plus complexes à intégrer, nécessitant des configurations spécifiques et des connecteurs dédiés.


**Coût :**
- **PostgreSQL** est totalement open-source et gratuit, ce qui le rend le plus économique.
- **MySQL** et **MongoDB** sont également open-source et gratuits, bien que des services managés (comme MongoDB Atlas) puissent entraîner des coûts.
- **SQL Server** et **Oracle** nécessitent des licences payantes, ce qui peut devenir très coûteux à grande échelle.


**Communauté et Support :**
- **MongoDB** et **PostgreSQL** ont des communautés très actives et une documentation exhaustive.
- **MySQL** dispose également d'une large communauté, mais elle est plus orientée vers des cas d'utilisation simples.
- **SQL Server** et **Oracle** bénéficient d'un bon support commercial, mais la documentation communautaire est plus limitée.


**Fonctionnalités spécifiques :**
- **Oracle** excelle dans les fonctionnalités avancées, notamment la gestion des transactions complexes, les partitionnements de données, et les types de données spécifiques.
- **SQL Server** et **PostgreSQL** offrent des fonctionnalités avancées pour les transactions et la gestion des données.
- **MongoDB** est plus limité dans les fonctionnalités transactionnelles, mais excellent pour des structures de données flexibles et des applications qui nécessitent de la scalabilité horizontale.
- **MySQL** est plus simple et moins riche en fonctionnalités avancées par rapport aux autres bases de données.


## Choix

En vue de la note finale du benchmark, notre choix s'est porté sur PostgreSql, aussi parce qu'il est idéal pour des besoins avancés en termes de sécurité et de transactions.